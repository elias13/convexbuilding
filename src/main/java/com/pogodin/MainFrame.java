package com.pogodin;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.HeadlessException;

public class MainFrame extends JFrame{


    public MainFrame(){
        PainFrame mainPanel = new PainFrame();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(mainPanel.getBounds());
        add(mainPanel);
        setVisible(true);

    }

    public static void main (String [] args){
        new MainFrame();
    }
}
