package com.pogodin;


import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

public class PainFrame extends JPanel implements  KeyListener{

    public static int MULTIPLIER = 10;

    List<Point> points ;
    List<Point> lines;
    Set<Point> allPoints;
    Set<Point> sellPoints;
    private static int pointSize = 10;

    public PainFrame(){
        this.setBounds(300,300,400,400);
        points = new ArrayList<Point>();
        lines = new ArrayList<Point>();
        allPoints = new HashSet<Point>();
        addMouseListener(mouseL);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawPoins(g);
        drawLines(g);
        fillAllPoints(g);

    }

    private void drawLines(Graphics g){
        if(lines == null || lines.size() < 2){
            return;
        }
        g.setColor(Color.BLACK);
        if(lines.size() > 2){
            for(int i = 1; i < lines.size(); i++){
                drawLine(g, lines.get(i-1), lines.get(i));
            }
        }
        drawLine(g, lines.get(0), lines.get(lines.size() - 1));

        for(int i = 0 ; i < 40; i ++){
            for(int j = 0; j < 40; j ++){
                g.drawRect(i * MULTIPLIER, j * MULTIPLIER, MULTIPLIER, MULTIPLIER);
            }
        }

    }

    private void drawLine(Graphics g, Point a, Point b){
        drawBresenhamLine(a.getX(), a.getY(), b.getX(), b.getY(), g, MULTIPLIER);
        /*g.drawLine(a.getX() + pointSize/2, a.getY() + pointSize/2,
                b.getX() + pointSize/2, b.getY() + pointSize/2);*/
    }

    private void drawPoins(Graphics g){
        Color savedColor = g.getColor();
        Random rand = new Random();
        g.setColor(Color.BLACK);
        for (Point point : points) {
            //g.setColor(new Color(rand.nextInt(128) + 128,rand.nextInt(256),rand.nextInt(256)));
            g.fillOval(point.getX(), point.getY(), pointSize, pointSize);
        }
        g.setColor(savedColor);
    }

    private void fillAllPoints(Graphics g){
        g.setColor(Color.BLACK);
        for(Point point: allPoints){
            g.fillRect(point.getX()*MULTIPLIER, point.getY()*MULTIPLIER,
                    MULTIPLIER, MULTIPLIER);
        }
    }




    private MouseListener mouseL = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            int buttonClicked = e.getButton();
            if(buttonClicked == MouseEvent.BUTTON1){
                    points.add(new Point(e.getX() - pointSize / 2, e.getY() - pointSize / 2));
                    repaint();
            } else {
                if(buttonClicked == MouseEvent.BUTTON3){
                    if(allPoints.isEmpty()){
                        return;
                    }
                    points = new ArrayList<Point>();
                    allPoints = LogicPart.getInstance().fill(e.getX()/MULTIPLIER, e.getY()/MULTIPLIER, allPoints);

                    JOptionPane.showMessageDialog(PainFrame.this, "Perimeter is : "
                            + LogicPart.getInstance().getPerimeter(allPoints));
                    repaint();
                }
            }

            System.out.println(e.getX() + "  " + e.getY());
        }

        @Override
        public void mousePressed(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    };


    public void drawBresenhamLine (int xstart, int ystart, int xend, int yend, Graphics g, int multiplier)
    /**
     * xstart, ystart - начало;
     * xend, yend - конец;
     * "g.drawLine (x, y, x, y);" используем в качестве "setPixel (x, y);"
     * Можно писать что-нибудь вроде g.fillRect (x, y, 1, 1);
     */
    {


        int x, y, dx, dy, incx, incy, pdx, pdy, es, el, err;

        xstart /= multiplier;
        ystart /= multiplier;
        xend /= multiplier;
        yend /= multiplier;

        dx = xend - xstart;//проекция на ось икс
        dy = yend - ystart;//проекция на ось игрек

        incx = sign(dx);
        /*
         * Определяем, в какую сторону нужно будет сдвигаться. Если dx < 0, т.е. отрезок идёт
         * справа налево по иксу, то incx будет равен -1.
         * Это будет использоваться в цикле постороения.
         */
        incy = sign(dy);
        /*
         * Аналогично. Если рисуем отрезок снизу вверх -
         * это будет отрицательный сдвиг для y (иначе - положительный).
         */

        if (dx < 0) dx = -dx;//далее мы будем сравнивать: "if (dx < dy)"
        if (dy < 0) dy = -dy;//поэтому необходимо сделать dx = |dx|; dy = |dy|
        //эти две строчки можно записать и так: dx = Math.abs(dx); dy = Math.abs(dy);

        if (dx > dy)
        //определяем наклон отрезка:
        {
            /*
            * Если dx > dy, то значит отрезок "вытянут" вдоль оси икс, т.е. он скорее длинный, чем высокий.
            * Значит в цикле нужно будет идти по икс (строчка el = dx;), значит "протягивать" прямую по иксу
            * надо в соответствии с тем, слева направо и справа налево она идёт (pdx = incx;), при этом
            * по y сдвиг такой отсутствует.
            */
            pdx = incx;
            pdy = 0;
            es = dy;
            el = dx;
        }
        else//случай, когда прямая скорее "высокая", чем длинная, т.е. вытянута по оси y
        {
            pdx = 0;
            pdy = incy;
            es = dx;
            el = dy;//тогда в цикле будем двигаться по y
        }

        x = xstart;
        y = ystart;
        err = el/2;
        g.fillRect(x*multiplier, y*multiplier, multiplier, multiplier);
        allPoints.add(new Point(x,y));
        //все последующие точки возможно надо сдвигать, поэтому первую ставим вне цикла

        for (int t = 0; t < el; t++)//идём по всем точкам, начиная со второй и до последней
        {
            err -= es;
            if (err < 0)
            {
                err += el;
                x += incx;//сдвинуть прямую (сместить вверх или вниз, если цикл проходит по иксам)
                y += incy;//или сместить влево-вправо, если цикл проходит по y
            }
            else
            {
                x += pdx;//продолжить тянуть прямую дальше, т.е. сдвинуть влево или вправо, если
                y += pdy;//цикл идёт по иксу; сдвинуть вверх или вниз, если по y
            }

            g.fillRect(x*multiplier, y*multiplier, multiplier, multiplier);
            allPoints.add(new Point(x,y));
        }
    }

    private int sign (int x) {
        return (x > 0) ? 1 : (x < 0) ? -1 : 0;
        //возвращает 0, если аргумент (x) равен нулю; -1, если x < 0 и 1, если x > 0.
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == e.VK_ENTER){
            lines = points;
            repaint();
        }
        if(e.getKeyCode() == e.VK_SHIFT){
            points = new ArrayList<Point>();
            lines = new ArrayList<Point>();
            allPoints = new HashSet<Point>();
            repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
