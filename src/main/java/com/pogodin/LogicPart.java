package com.pogodin;

import java.util.*;

public class LogicPart {

    private static LogicPart instance;

    private LogicPart(){}

    public static LogicPart getInstance(){
        if(instance == null){
            instance = new LogicPart();
        }
        return instance;
    }

    public List<Point> getResultFigure(final List<Point> allPoints){
        List<Point> resultFigure = new ArrayList<Point>();
        List<Point> allPointsCopy = new ArrayList<Point>(new HashSet<Point>(allPoints));
        Point leftest = getTheLeftest(allPoints);
        resultFigure.add(leftest);
        allPointsCopy.remove(leftest);

        resultFigure.addAll(sortIt(allPointsCopy, leftest));
        System.out.println("resultF : " + resultFigure);
        resultFigure = fixTheFigureShell(resultFigure);

        return resultFigure;
    }

    private List<Point> sortIt(final List<Point> points, final Point leftest){
        List<Point> sorted = new ArrayList<Point>(points);

        Collections.sort(sorted, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return -rotate(leftest, o2, o1);
            }
        });
        return sorted;
    }

    private List<Point> fixTheFigureShell(List<Point> points){
        if(points == null || points.size() < 4){
            return points;
        }
        for(int i = 2; i < points.size(); i++){
            if(rotate(points.get(i-2), points.get(i-1), points.get(i)) > 0){
                points.remove(i - 1);
                i--;
            }
        }
        return points;
    }

    private Point getTheLeftest(List<Point> points){
        if(points == null){
            return null;
        }
        Point leftest = points.get(0);
        for (Point point : points) {
            if(point.getX() < leftest.getX()){
                leftest = point;
            }
        }
        return leftest;
    }

    public int rotate(Point a, Point b, Point c){
        return (b.getX() - a.getX())*(c.getY() - b.getY()) - (b.getY() - a.getY())*(c.getX() - b.getX());
    }

    public Set<Point> fill(int x, int y, Set<Point> points){
        if(x < 0 || y < 0 || x > 400 || y > 400){
            return points;
        }

        if(!points.contains(new Point(x,y))){
            points.add(new Point(x,y));

            points.addAll(fill(x + 1, y, points));
            points.addAll(fill(x - 1, y, points));
            points.addAll(fill(x, y + 1, points));
            points.addAll(fill(x, y - 1, points));
        }
        return points;
    }

    public int getPerimeter(Set<Point> allPoints){
        int perimeter = 0;
        for (Point point : allPoints) {
            if(!allPoints.contains(new Point(point.getX() - 1, point.getY()))){
                perimeter++;
            }
            if(!allPoints.contains(new Point(point.getX() + 1, point.getY()))){
                perimeter++;
            }
            if(!allPoints.contains(new Point(point.getX(), point.getY() + 1))){
                perimeter++;
            }
            if(!allPoints.contains(new Point(point.getX(), point.getY() - 1))){
                perimeter++;
            }
        }
        return perimeter;
    }
}