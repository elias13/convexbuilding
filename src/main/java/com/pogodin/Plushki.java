package com.pogodin;

import javax.swing.JPanel;

public class Plushki {

    private JPanel panel;
    private volatile boolean isGarland = false;

    private static Plushki instance;

    private Plushki(){}

    public static Plushki getI(){
        return instance;
    }

    public static Plushki getInstance() {
        if(instance == null){
            instance = new Plushki();
        }
        return instance;
    }

    public void startGarland(final JPanel panel, int millis){
        isGarland = true;
        final int innerMillis = millis == 0 ? 1000 : millis;
        Runnable runable = new Runnable() {
            @Override
            public void run() {
                while(isGarland){
                    panel.repaint();
                    try {
                        Thread.sleep(innerMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        };
        new Thread(runable).start();
    }

    public void stopGarland(){
        isGarland = false;
    }


}
